# Load libraries
from sklearn.ensemble import AdaBoostClassifier

# Import Support Vector Classifier
from sklearn.svm import SVC
#Import scikit-learn metrics module for accuracy calculation
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from rhorho import RhoRhoEvent
from data_utils import read_np, EventDatasets
import numpy as np
from sklearn.metrics import roc_auc_score
import os
from sklearn.datasets import load_iris
from IPython.display import Image
from sklearn.externals.six import StringIO  
import pydot
import collections
from sklearn.tree import DecisionTreeClassifier, export_graphviz


from sklearn.metrics import accuracy_score
from sklearn import tree
from subprocess import check_call

import datetime
import time



class DecisionTree(object):
    def __init__(self, tree=3):
    	self.model = DecisionTreeClassifier(criterion = "gini", random_state = 10,
                               max_depth=tree, min_samples_leaf=5)

#AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),algorithm="SAMME",n_estimators=200)

    def train(self, dataset):
        n = len(dataset.x)
        y = np.concatenate([np.ones(n), np.zeros(n)])
        clf = self.model.fit(np.concatenate([dataset.x, dataset.x]), y, sample_weight=np.concatenate([dataset.wa, dataset.wb]))
	predictions = self.model.predict(dataset.x)
	auc = roc_auc_score(y, np.concatenate([predictions, predictions]), sample_weight=np.concatenate([dataset.wa, dataset.wb]))
	print("Train AUC: %.2f%%" % (auc * 100.0))


	#export_graphviz(clf, 
                #out_file='tree.dot', 
                #feature_names = self.model.feature_names,
                #class_names = self.model.target_names,
                #rounded = True, proportion = False, 
                #precision = 2, filled = True)
	#check_call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=600'])
	#Image(filename = 'tree.png')
	
	

    def test(self, dataset):
        n = len(dataset.x)
        y = np.concatenate([np.ones(n), np.zeros(n)])
        predictions = self.model.predict(dataset.x)
        # predictions = [round(value) for value in self.model.predict(dataset.x)]
        auc = roc_auc_score(y, np.concatenate([predictions, predictions]), sample_weight=np.concatenate([dataset.wa, dataset.wb]))
        print("AUC: %.2f%%" % (auc * 100.0))
    
	

def run(args):
    data_path = args.IN

    print "Loading data"
    data = read_np(os.path.join(data_path, "rhorho_raw.data.npy"))
    w_a = read_np(os.path.join(data_path, "rhorho_raw.w_a.npy"))
    w_b = read_np(os.path.join(data_path, "rhorho_raw.w_b.npy"))
    perm = read_np(os.path.join(data_path, "rhorho_raw.perm.npy"))
    print "Read %d events" % data.shape[0]

    print "Processing data"
    event = RhoRhoEvent(data, args)
    points = EventDatasets(event, w_a, w_b, perm)

    num_features = points.train.x.shape[1]
    print "Generated %d features" % num_features

    model = DecisionTree(tree=args.TREE)
    model.train(points.train)
    model.test(points.valid)
    model.test(points.test)
    
    ts2 = time.time()
    st2 = datetime.datetime.fromtimestamp(ts2).strftime('%Y-%m-%d %H:%M:%S')
    print st2

def start(args):
    run(args)

if __name__ == "__main__":
    start(args = {})
