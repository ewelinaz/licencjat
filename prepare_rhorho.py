import numpy as np
from helpful_functions import read_raw_root
from helpful_functions import find_first_line
import argparse
import os


def read_raw_all(kind, args):
    #Path to data
    path_to_data = args.IN
    #Tables of data and weights
    all_data = []
    all_weights = []
    #Find file
    for letter in ["a", "b", "c", "d", "e", "f", "g", "h", "i", "k"][:args.DATASETS]:
        name = os.path.join(path_to_data, "pythia.H.rhorho.1M.%s.%s.outTUPLE_labFrame" % (letter, kind))
        print letter, name
        data, weights = read_raw_root(name, num_particles=7)
        #Tables of data and weights
        all_data += [data]
        all_weights += [weights]
    #concatenate data
    all_data = np.concatenate(all_data)
    all_weights = np.concatenate(all_weights)
    return all_data, all_weights


if __name__ == "__main__":
    #parser = argparse.ArgumentParser(description='Train classifier')
    parser.add_argument("-d", "--datasets", dest="DATASETS", default=1, type=int, help="number of datasets to prepare")
    parser.add_argument("-i", "--input", dest="IN", help="name of input folder")
    args = parser.parse_args()
    #Reading scalar and pseudoscalar
    data_a, weights_a = read_raw_all("scalar", args)
    data_b, weights_b = read_raw_all("pseudoscalar", args)

    print "Total number of examples: %d" % len(weights_a)
    np.testing.assert_array_almost_equal(data_a, data_b)
    #Random number 
    np.random.seed(123)
    perm = np.random.permutation(len(weights_a))
    #Pat to data , IN argument 
    path_to_data = args.IN
    #Save 
    np.save(os.path.join(path_to_data, "rhorho_raw.data.npy"), data_a)
    np.save(os.path.join(path_to_data, "rhorho_raw.w_a.npy"), weights_a)
    np.save(os.path.join(path_to_data, "rhorho_raw.w_b.npy"), weights_b)
    np.save(os.path.join(path_to_data, "rhorho_raw.perm.npy"), perm)
