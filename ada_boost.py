# Load libraries
from sklearn.ensemble import AdaBoostClassifier

# Import Support Vector Classifier
from sklearn.svm import SVC
#Import scikit-learn metrics module for accuracy calculation
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from rhorho import RhoRhoEvent
from data_utils import read_np, EventDatasets
import numpy as np
from sklearn.metrics import roc_auc_score
import os


from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
import datetime
import time

# Import Support Vector Classifier
from sklearn.svm import SVC
#Import scikit-learn metrics module for accuracy calculation
from sklearn import metrics
svc=SVC(probability=True, kernel='linear')




class AdaBoost(object):
    def __init__(self, tree=5):
	#self.model = AdaBoostClassifier(n_estimators=estimator, base_estimator=svc,learning_rate=1)
    	self.model = AdaBoostClassifier(DecisionTreeClassifier(max_depth=tree),algorithm="SAMME",n_estimators=20 )

    def train(self, dataset):
        n = len(dataset.x)
        y = np.concatenate([np.ones(n), np.zeros(n)])
        self.model.fit(np.concatenate([dataset.x, dataset.x]), y, sample_weight=np.concatenate([dataset.wa, dataset.wb]))
	predictions = self.model.predict(dataset.x)
	auc = roc_auc_score(y, np.concatenate([predictions, predictions]), sample_weight=np.concatenate([dataset.wa, dataset.wb]))
	print("Train AUC: %.2f%%" % (auc * 100.0))

    def test(self, dataset):
        n = len(dataset.x)
        y = np.concatenate([np.ones(n), np.zeros(n)])
        predictions = self.model.predict(dataset.x)
        # predictions = [round(value) for value in self.model.predict(dataset.x)]
       # accuracy = accuracy_score(y, np.concatenate([predictions, predictions]), sample_weight=np.concatenate([dataset.wa, dataset.wb]))
        #print("Accuracy: %.2f%%" % (accuracy * 100.0))
        auc = roc_auc_score(y, np.concatenate([predictions, predictions]), sample_weight=np.concatenate([dataset.wa, dataset.wb]))
        print("AUC: %.2f%%" % (auc * 100.0))

def run(args):
    data_path = args.IN

    print "Loading data"
    data = read_np(os.path.join(data_path, "rhorho_raw.data.npy"))
    w_a = read_np(os.path.join(data_path, "rhorho_raw.w_a.npy"))
    w_b = read_np(os.path.join(data_path, "rhorho_raw.w_b.npy"))
    perm = read_np(os.path.join(data_path, "rhorho_raw.perm.npy"))
    print "Read %d events" % data.shape[0]

    print "Processing data"
    event = RhoRhoEvent(data, args)
    points = EventDatasets(event, w_a, w_b, perm)

    num_features = points.train.x.shape[1]
    print "Generated %d features" % num_features

    model = AdaBoost(tree=args.TREEDEPTHA)
    model.train(points.train)
    model.test(points.valid)
    model.test(points.test)

    ts2 = time.time()
    st2 = datetime.datetime.fromtimestamp(ts2).strftime('%Y-%m-%d %H:%M:%S')
    print st2

def start(args):
    run(args)

if __name__ == "__main__":
    start(args = {})
