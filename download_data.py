import os
import argparse
import requests 


parser = argparse.ArgumentParser(description='Train classifier')
parser.add_argument("-d", dest="DATASETS", type=int, default='1', help="number of datasets to download")
parser.add_argument("-o", dest="OUT", default=os.environ["RHORHO_DATA"])
parser.add_argument("-s", dest="SOURCE", default=os.environ["DATA_SOURCE"])

args = parser.parse_args()

source = args.SOURCE
rhorho_path = args.OUT

url = "http://th-www.if.uj.edu.pl/~erichter/forEwelina/HiggsCP_data/"

for letter in ["a", "b", "c", "d", "e", "f", "g", "h", "i", "k"][:args.DATASETS]:
    for type_of_data in ["scalar", "pseudoscalar"]:
	name = "pythia.H.rhorho.1M.%s.%s.outTUPLE_labFrame" % (letter, type_of_data)
	if os.path.exists(os.path.join(rhorho_path,  name)):
	    print name, " is already downloaded."
	    continue
	file_url = source + name
	print file_url
	
 
	r = requests.get(file_url, stream = True) 
	try:
        	if not os.path.exists(rhorho_path):
           		os.makedirs(rhorho_path)
    	except OSError:
		print ('Error: Creating directory. ' + directory)

	with open(rhorho_path+"/"+name,"wb+") as pdf: 
	    
	    for chunk in r.iter_content(chunk_size=1024): 
	  
		 # writing one chunk at a time to pdf file 
		 if chunk: 
		     pdf.write(chunk) 
