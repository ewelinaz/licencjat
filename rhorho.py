import numpy as np
from particle import Particle
from math_utils import *
from particle import *
import matplotlib.pyplot as plt
from numpy import mean, sqrt, square
from histogram import*

class RhoRhoEvent(object):
    def __init__(self, data, args, debug=True):
        # [n, pi-, pi0, an, pi+, pi0]

        p = [Particle(data[:, 5 * i:5 * i + 4]) for i in range(6)]
        cols = []

        def get_tau1(p):
            tau1_nu = p[0]
            tau1_pi = p[1:3]
            tau1_rho = tau1_pi[0] + tau1_pi[1]
            tau1 = tau1_rho + tau1_nu  # nowe

            return tau1_nu, tau1_pi, tau1_rho, tau1

        def get_tau2(p):
            tau2_nu = p[3]
            tau2_pi = p[4:6]
            tau2_rho = tau2_pi[0] + tau2_pi[1]
            tau2 = tau2_rho + tau2_nu  # nowe

            return tau2_nu, tau2_pi, tau2_rho, tau2


        p_tau1_nu, l_tau1_pi, p_tau1_rho, p_tau1 = get_tau1(p)  # p- particle, l-list
        p_tau2_nu, l_tau2_pi, p_tau2_rho, p_tau2 = get_tau2(p)

        rho_rho = p_tau1_rho + p_tau2_rho

        PHI, THETA = calc_angles(p_tau1_rho, rho_rho)
        

        # all particles boosted & rotated
        for i, idx in enumerate([0, 1, 2, 3, 4, 5]):
            part = boost_and_rotate(p[idx], PHI, THETA, rho_rho)
            
            if args.FEAT in ["Model-OnlyHad", "Variant-4.0", "Variant-4.1"]:
                if idx not in [0, 3]:
                    cols.append(part.vec)
            if args.FEAT == "Variant-ALL":
                cols.append(part.vec)

        if args.FEAT == "Variant-4.0":
            part = boost_and_rotate(p_tau1, PHI, THETA, rho_rho)
            cols.append(part.vec)
            part = boost_and_rotate(p_tau2, PHI, THETA, rho_rho)
            cols.append(part.vec)

        if args.FEAT == "Variant-4.1":
            lifetime = .08711
            #scale p_tau1
            p_tau1_sc = p_tau1.scale_vec(lifetime)

            #get each direction of 4-vector, before and after scale
            x_p_tau1 = p_tau1.getarrayx()
            x_p_tau1_sc = p_tau1_sc.getarrayx()

	    y_p_tau1 = p_tau1.getarrayy()
            y_p_tau1_sc = p_tau1_sc.getarrayy()

	    z_p_tau1 = p_tau1.getarrayz()
            z_p_tau1_sc = p_tau1_sc.getarrayz()

	    e_p_tau1 = p_tau1.getarraye()
            e_p_tau1_sc = p_tau1_sc.getarraye()

	    #save data to txt file
	    #np.savetxt('x_p_tau1.txt', x_p_tau1, delimiter=',')
            #np.savetxt('x_p_tau1_sc.txt', x_p_tau1_sc, delimiter=',')
	    #formated data saved to txt file
            #DAT = np.column_stack((x_p_tau1_sc, x_p_tau1))
            #np.savetxt('formated.txt', DAT, delimiter=",")
            #np.savetxt('formated1.txt', DAT, delimiter=" ")
            # print "x p_tau1 ", x_p_tau1
            # print "x p_tau1_sc ", x_p_tau1_sc

            part = boost_and_rotate(p_tau1, PHI, THETA, rho_rho)
            cols.append(part.vec)
            p_tau2 = p_tau2.scale_vec(lifetime)
            part = boost_and_rotate(p_tau2, PHI, THETA, rho_rho)
            cols.append(part.vec)

            #divide scaled p / p
            divided_tau1_x = np.true_divide(x_p_tau1_sc, x_p_tau1)
            divided_tau1_y = np.true_divide(y_p_tau1_sc, y_p_tau1)
            divided_tau1_z = np.true_divide(z_p_tau1_sc, z_p_tau1)
            divided_tau1_e = np.true_divide(e_p_tau1_sc, e_p_tau1)

            #print divided_tau1
	    #generateHistogram(divided_tau1_e)
	    #generateHistogram(divided_tau1_x)
	    #generateHistogram(divided_tau1_y)
	    #generateHistogram(divided_tau1_z)
            
 

        # rho particles & recalculated mass
        if args.FEAT == "Model-OnlyHad":
            for i, rho in enumerate([p_tau1_rho] + [p_tau2_rho]):
                rho = boost_and_rotate(rho, PHI, THETA, rho_rho)
                # rho = rho
                cols.append(rho.vec)
                cols.append(rho.recalculated_mass)

        if args.FEAT == "Model-OnlyHad":
            cols += [get_acoplanar_angle(p[1], p[2], p[4], p[5], rho_rho)]
            cols += [get_y(p[1], p[2], rho_rho), get_y(p[4], p[5], rho_rho)]

  

        # filter
        filt = (p_tau1_rho.pt >= 20)
        for part in (l_tau1_pi + l_tau2_pi):
            filt = filt & (part.pt >= 1)
        filt = filt.astype(np.float32)

        if args.FEAT in ["Variant-ALL", "Model-OnlyHad", "Variant-4.0", "Variant-4.1"]:
            cols += [filt]

        for i in range(len(cols)):
            if len(cols[i].shape) == 1:
                cols[i] = cols[i].reshape([-1, 1])

        print cols

        self.cols = np.concatenate(cols, 1)
