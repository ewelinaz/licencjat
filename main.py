import argparse

import train_boostedtrees, ada_boost, train_decisiontree, train_gradient

types = {"boosted_trees": train_boostedtrees.start, "ada_boost": ada_boost.start, "decision_tree" : train_decisiontree.start , "gradient_boosting" : train_gradient.start}

parser = argparse.ArgumentParser(description='Train classifier')
parser.add_argument("-t", "--type", dest="TYPE", choices=types.keys(), default='nn_rhorho', 	
	help = "type of classification method")
parser.add_argument("-i", "--input", dest="IN", required=True, help="name of input folder")
parser.add_argument("-f", "--features", dest="FEAT", help="Features",
	choices=["Variant-ALL", "Model-OnlyHad", "Variant-4.0", "Variant-4.1"])
parser.add_argument("--treedepth", dest="TREEDEPTH", type=int, default=5 , help="Three depth")
parser.add_argument("--treedepthgbm", dest="TREEDEPTHGBM", type=int, default=5 , help="Three depth")
parser.add_argument("-e", dest="TREE", type=int, default=3 , help="Tree")
parser.add_argument("--treedeptha", dest="TREEDEPTHA", type=int, default=5 , help="Three depth")



args = parser.parse_args()

types[args.TYPE](args)
