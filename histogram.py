import numpy as np
from particle import Particle
from math_utils import *
from particle import *
import matplotlib.pyplot as plt
from numpy import mean, sqrt, square
from rhorho import *

def generateHistogram(divided_tau):
	
	hist, bins_edges = np.histogram(divided_tau)
	n, bins, patches = plt.hist(x=divided_tau, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85)	    
	plt.grid(axis='y', alpha=0.75)

	plt.title(' Histogram p_tau1_sc / p_tau1 dla skladowej e')
	plt.show()
	return plt.show()
"""
def calculate():
	s = 0
	for i in range(len(n)):
	s += n[i] * ((bins[i] + bins[i + 1]) / 2)
	mean = s / np.sum(n)
	print mean
		    

	t = 0
	for i in range(len(n)):
	t += (bins[i] - mean) ** 2
	std = np.sqrt(t / np.sum(n))
	print std
		
		    
	rms(divided_tau1)
	print "rms" ,  rms(divided_tau1)"""

